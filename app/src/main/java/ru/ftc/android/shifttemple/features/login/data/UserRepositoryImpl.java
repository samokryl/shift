package ru.ftc.android.shifttemple.features.login.data;

import java.util.ArrayList;
import java.util.List;

import ru.ftc.android.shifttemple.features.login.domain.UserRepository;
import ru.ftc.android.shifttemple.features.login.domain.model.User;

public final class UserRepositoryImpl implements UserRepository {

    @Override
    public List<User> getUserList() {
        final List<User> userList = new ArrayList<>();

        userList.add(new User("UserA", " Петя"));
        userList.add(new User("UserB", " Вася"));
        userList.add(new User("UserC", " Катя"));

        return userList;
    }
}