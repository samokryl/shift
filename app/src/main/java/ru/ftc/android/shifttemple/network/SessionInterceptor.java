package ru.ftc.android.shifttemple.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.ftc.android.shifttemple.features.login.domain.SessionRepository;

public final class SessionInterceptor implements Interceptor {

    private static final String HEADER_USER_ID = "userId";

    private final SessionRepository sessionRepository;

    public SessionInterceptor(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        builder.header(HEADER_USER_ID, sessionRepository.getSessionId());
        return chain.proceed( builder.build());
    }
}