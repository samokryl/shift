package ru.ftc.android.shifttemple.features.login.domain;

public interface SessionRepository {

    String getSessionId();

    void setSessionId(String sessionId);
}
