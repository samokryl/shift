package ru.ftc.android.shifttemple.features.login.presentation;

import android.content.Context;

import ru.ftc.android.shifttemple.features.login.domain.SessionRepository;
import ru.ftc.android.shifttemple.features.login.data.SessionRepositoryImpl;
import ru.ftc.android.shifttemple.features.login.domain.UserRepository;
import ru.ftc.android.shifttemple.features.login.data.UserRepositoryImpl;

final class PresenterFactory {

    static LoginPresenter createPresenter(Context context) {
        final UserRepository userRepository = new UserRepositoryImpl();

        final SessionRepository sessionRepository = new SessionRepositoryImpl(context);

        return new LoginPresenter(userRepository, sessionRepository);
    }
}