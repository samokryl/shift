package ru.ftc.android.shifttemple.features.books.presentation;

import android.content.Context;

import ru.ftc.android.shifttemple.App;
import ru.ftc.android.shifttemple.features.books.data.BooksApi;
import ru.ftc.android.shifttemple.features.books.domain.BooksRepository;
import ru.ftc.android.shifttemple.features.books.data.BooksRepositoryImpl;

/**
 * Created: samokryl
 * Date: 02.07.18
 * Time: 1:03
 */

final class PresenterFactory {

    static BookListPresenter createPresenter(Context context) {
        final BooksApi api = App.getRetrofitProvider(context)
                .getRetrofit()
                .create(BooksApi.class);

        final BooksRepository repository = new BooksRepositoryImpl(api);

        return new BookListPresenter(repository);
    }

}