package ru.ftc.android.shifttemple.features.login.presentation;

import java.util.List;

import ru.ftc.android.shifttemple.features.MvpPresenter;
import ru.ftc.android.shifttemple.features.login.domain.SessionRepository;
import ru.ftc.android.shifttemple.features.login.domain.UserRepository;
import ru.ftc.android.shifttemple.features.login.domain.model.User;

public final class LoginPresenter extends MvpPresenter<LoginView> {

    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;

    LoginPresenter(UserRepository userRepository, SessionRepository sessionRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }

    @Override
    protected void onViewReady() {
        final List<User> userList = userRepository.getUserList();
        view.showUserList(userList);
    }

    void onNavigateNextClick(User selectedUser) {
        if (selectedUser == null) {
            view.showNotSelectedUserError();
        } else {
            sessionRepository.setSessionId(selectedUser.getSessionId());
            view.openBookListScreen();
        }
    }
}